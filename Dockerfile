FROM python:3.8-slim-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /python

COPY ./requirements.txt /python
COPY . . /python/

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN python manage.py migrate


EXPOSE 5000

CMD ["python", "manage.py", "runserver", "0.0.0.0:5000"]
